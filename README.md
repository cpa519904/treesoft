# treesoft

#### 项目介绍
TreeSoft数据库管理系统 TreeDMS V2.3.1 反编译工程优化代码
1. 转换为springboot工程
2. 数据库改为mysql


项目演示地址：  https://soft.00fly.online/treesoft/login  （用户名、密码无需输入，直接点击登录）

已经配置了一个数据库 **演示用，勿破坏，mysql 跑在docker容器中，你破坏了也没啥用，treesoft开头的表勿动勿删，别的随意** 

master分支为springmvc、springboot工程。

#### docker相关

1. 项目已经启用docker-maven-plugin，打包镜像时，在treesoft 目录执行 `mvn clean package docker:build`，前提条件是系统已经安装maven、docker。

2. 执行第一步操作后，会在target/docker生成war并拷贝${project.basedir}/docker下的文件，lunix环境下执行`sh restart.sh`重启应用，`sh stop.sh`停止应用。

   

